
import mopynaco

from distutils.core import setup

version = '13.21'

description = mopynaco.__doc__.strip()
long_description = '''Generic python libraries.'''

settings = dict(

    name = 'python3-mopynaco',
    version = version,

    packages = ['mopynaco', 'mopynaco.props'],

    author = 'Florian Haftmann',
    author_email = 'florian.haftmann@informatik.tu-muenchen.de',
    description = description,
    long_description = long_description,

    license = 'EUPL and GPL',
    url = 'http://bitbucket.org/haftmann/mopynaco'

)

setup(**settings)
