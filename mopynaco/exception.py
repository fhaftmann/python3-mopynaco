
'''
    Exception gestures.
'''

# Copyright 2012 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['produce', 'Wrapping_Exception', 'Cascading_Exception', 'reporting', 'logging',
  'logging_abort', 'Invocation_Exception', 'Argument_Parser', 'masking_value_error',
  'freeze', 'thaw']

from contextlib import contextmanager
import argparse
import sys
import traceback

from .Sum import Sum


def produce(Exc, *args, **kwargs):

    """Functional counterpart for the raise statement."""

    raise Exc(*args, **kwargs)


def short_message_for(exc):

    return ''.join(traceback.format_exception_only(type(exc), exc)).rstrip()


class Wrapping_Exception(Exception):

    """Exception wrapping another exception with cumulative messaging."""

    def __init__(self, exc, message):

        self.exc = exc
        self.message = message

    def __str__(self):

        return self.message + ': ' + short_message_for(self.exc)

    @classmethod
    def wrapup(Wrapping_Exception, *args):

        """Wrapup currently pending exception into Wrapping_Exception with arguments args."""

        _, exc_value, exc_traceback = sys.exc_info()
        raise Wrapping_Exception(exc_value, *args).with_traceback(exc_traceback)

    @classmethod
    @contextmanager
    def ascend(Wrapping_Exception, *args):

        try:
            yield
        except Exception:
            _, exc_value, exc_traceback = sys.exc_info()
            raise Wrapping_Exception(exc_value, *args).with_traceback(exc_traceback)


class Cascading_Exception(Exception):

    """Exception with cascading message augmentation."""

    def __init__(self, base_message, cascade = []):

        assert isinstance(base_message, str)
        self.base_message = base_message
        self.cascade = cascade

    def format_cascade(self, cascade):

        return ': ' + ', '.join(cascade)

    def __str__(self):

        return self.base_message + self.format_cascade(self.cascade)

    @classmethod
    @contextmanager
    def ascend(Cascading_Exception, txt):

        assert isinstance(txt, str)
        try:
            yield
        except Cascading_Exception:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            raise Cascading_Exception(exc_value.base_message, [txt] + exc_value.cascade).with_traceback(exc_traceback)


@contextmanager
def reporting(f, Exc = Exception):

    """Chain an error-reporting function into a traceback."""

    try:
        yield
    except Exc as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        msg = traceback.format_exception(exc_type, exc_value, exc_traceback)
        try:
            f(exc_type, exc_value, exc_traceback, msg)
        except Exception as e2:
            raise Wrapping_Exception(e, short_message_for(e2) + ', while reporting exception').with_traceback(exc_traceback)
        else:
            raise


@contextmanager
def logging(f, Exc = Exception):

    """Chain a logging function into a traceback."""

    def _f(exc_type, exc_value, exc_traceback, msg):
        f(''.join(msg))

    with reporting(_f, Exc):
        yield


@contextmanager
def logging_abort(f, Exc = Exception, exit_value = 2, prefix = 'Error: '):

    """Catch a (user-level) exception, log and exit with specified exit value."""

    from . import text ## avoid mutual recursive import at compile time

    def _f(exc_type, exc_value, exc_traceback, msg):
        print(prefix + text.of_anything(exc_value), file = sys.stderr)
        f(''.join(msg))
        raise SystemExit(exit_value)

    with reporting(_f, Exc):
        yield


class Invocation_Exception(Exception):

    """Bad command line invocation."""


class Argument_Parser(argparse.ArgumentParser):

    def error(self, message):
        raise Invocation_Exception(message + '\n\n' + str(self.format_usage()))


@contextmanager
def masking_value_error(f):

    """Substitute ValueError by a specific error message."""

    try:
        yield
    except ValueError:
        _, _, exc_traceback = sys.exc_info()
        raise ValueError(f()).with_traceback(exc_traceback)


# frozen exceptions as explicit values

class Result(Sum('none', 'result', 'exception')): pass

def freeze(f, *args, **kwargs):

    try:
        result = f(*args, **kwargs)
        return Result(none = ()) if result is None else Result(result = result)
    except BaseException:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        return Result(exception = (exc_type, exc_value, exc_traceback))


def thaw(result):

    assert isinstance(result, Result)

    if result.result is not None:
        return result.result
    elif result.none is not None:
        return
    else:
        exc_type, exc_value, exc_traceback = result.exception
        raise exc_value.with_traceback(exc_traceback)
