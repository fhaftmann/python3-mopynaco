
'''
    Isabelle-style antiquotations.
'''

# Copyright 2012 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['Antiquote_Exception', 'simple', 'extended']


import re

from . import seq
from . import text
from .exception import Wrapping_Exception


pattern_antiquote = R'\{(?P<name>\w+?)(?P<args>(?:\s+\S+?)*?)\s*\}'
re_arg = re.compile(R'\s*(\S+)')


class Antiquote_Exception(Wrapping_Exception):

    def __init__(self, exc, markup_prefix, name, arguments):

        self.name = name
        self.arguments = arguments
        message = 'While processing antiquotation ' + markup_prefix \
          + '{' + ' '.join([txt for txt in (name,) + arguments]) + '}'
        super(Antiquote_Exception, self).__init__(exc, message)


def parse(markup_prefix, embed, join, txt):

    assert isinstance(txt, str)

    re_antiquote = re.compile(re.escape(markup_prefix) + pattern_antiquote)

    txts = []
    antiquote_exprs = []
    lastindex = 0
    for match in re_antiquote.finditer(txt):
        txts.append(txt[lastindex:match.start(0)])
        name = match.group('name')
        args = tuple(match.group(1) for match in re_arg.finditer(match.group('args')))
        antiquote_exprs.append((name, args))
        lastindex = match.end(0)
    txts.append(txt[lastindex:])

    def insert_antiquotes(antiquotes):

        assert len(antiquotes) == len(antiquote_exprs)

        return join(list(seq.flat([[embed(txt), value]
          for txt, value in zip(txts, antiquotes + [embed('')])])))

    return antiquote_exprs, insert_antiquotes


def join_text(txts):

    assert all(isinstance(txt, str) for txt in txts)
    return ''.join(txts)


def extended(markup_prefix, antiquotations, embed, join, txt):

    assert isinstance(txt, str)

    antiquote_exprs, insert_antiquotes = parse(markup_prefix, embed, join, txt)

    antiquotes = []
    for name, args in antiquote_exprs:
        if name not in antiquotations:
            raise KeyError(f'No such antiquotation: »{name}«')
        antiquotation = antiquotations[name]
        try:
            antiquote = antiquotation(*args)
        except Exception:
            Antiquote_Exception.wrapup(markup_prefix, name, args)
        antiquotes.append(antiquote)

    return insert_antiquotes(antiquotes)


def simple(antiquotations, txt):

    return extended(markup_prefix = '@',
      antiquotations = antiquotations, embed = lambda txt: txt,
      join = join_text, txt = txt)


antiquote = simple ## legacy access
