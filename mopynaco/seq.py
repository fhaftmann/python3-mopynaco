
'''
    Sequence utilities.
'''

# Copyright 2012 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['null', 'split', 'flat', 'prefixes', 'is_prefix_of',
  'slices',  'partition', 'common_prefix_suffixes', 'duplicates', 'distinct_list',
  'chunks', 'transpose', 'slider',
  'group', 'powerset_complement']


from . import arith


def null(xs):

    if hasattr(xs, '__len__'):
        return len(xs) == 0
    else:
        for _ in xs:
            return False
        return True


def split(xs):

    ys = []
    zs = []
    for y, z in xs:
        ys.append(y)
        zs.append(z)
    return ys, zs


def flat(xss):

    for xs in xss:
        for x in xs:
            yield x


def prefixes(xs):

    for n in range(len(xs)):
        yield xs[:n + 1]


def is_prefix_of(xs, ys):

    return ys[:len(xs)] == xs


def slices(xs, n):

    for m in range(0, len(xs), n):
        yield xs[m : m + n]


def chunks(xs, p):

    ys = []
    for x in xs:
        if p(x) and not null(ys):
            yield ys
            ys = []
        ys.append(x)
    if not null(ys):
        yield ys


def transpose(xss, neutral = None):

    n = max(len(xs) for xs in xss)
    for k in range(n):
        yield [xs[k] if k < len(xs) else neutral for xs in xss]


def slider(xs, width = 1):

    arith.assert_pos_nat(width)

    for k in range(len(xs) + 1 - width):
        yield xs[k : k + width]


def partition(p, xs):

    ys, zs = [], []
    for x in xs:
        if p(x):
            ys.append(x)
        else:
            zs.append(x)
    return (ys, zs)


def common_prefix_suffixes(xs, ys):

    zs = []
    n = 0
    n_max = min(len(xs), len(ys))
    while n < n_max and xs[n] == ys[n]:
        zs.append(xs[n])
        n += 1
    return zs, xs[n:], ys[n:]


def duplicates(xs):

    ys = set()
    zs = set()
    for x in xs:
        if x in ys and x not in zs:
            zs.add(x)
            yield x
        else:
            ys.add(x)


def distinct_list(xs):

    ys = []
    for x in xs:
        if x not in ys:
            ys.append(x)
    return ys


def group(items, seq = list):

    d = dict()
    for key, value in items:
        d.setdefault(key, []).append(value)
    for key, value in d.items():
        d[key] = seq(value)
    return d


def powerset_complement(xs):

    if null(xs):
        yield ([], [])
        return

    x = xs[0]
    for zs, ws in powerset_complement(xs[1:]):
        yield [x] + zs, ws
        yield zs, [x] + ws
