
'''
    Tactful employment of sudo user privileges.
'''

# Copyright 2011 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['Sudo_User']


from contextlib import contextmanager
import os
from os import path
import stat
import getpass
import subprocess
import pwd

from .Singleton import Singleton
from . import exclusive
from . import subproc
from . import pathutil
from . import default


host_user_logbase = '/var/log'


class Sudo_User(Singleton):

    def __init__(self):

        self.username = self.background_user()
        self.rootable = os.getuid() == 0
        _, _, self.user_uid, self.user_gid, _, _, _ = pwd.getpwnam(self.username)
        if self.rootable:
            os.initgroups(self.username, self.gid)
            self.user_umask = \
              int(subprocess.run(['su', '-s', '/usr/bin/python3', '-c', 'import os; print(os.umask(0))',
                '-l', '--', self.username], stdout = subprocess.PIPE).stdout)
            self.revoke_root()

        if 'SUDO_USER' in os.environ:
            self.root_environment = dict(os.environ)
            self.user_environment = dict(self.root_environment)
            for key in 'LOGNAME', 'USERNAME', 'USER':
                self.user_environment[key] = self.username
            for key in 'MAIL', 'SUDO_COMMAND', 'SUDO_GID', 'SUDO_UID', 'SUDO_USER':
                if key in self.user_environment:
                    del self.user_environment[key]
        else:
            self.user_environment = self.root_environment = dict(os.environ)

    @staticmethod
    def background_user():

        return os.environ.get('SUDO_USER', getpass.getuser())

    @property
    def uid(self):

        return self.user_uid

    @property
    def gid(self):

        return self.user_gid

    def revoke_root(self):

        os.setegid(self.gid)
        os.seteuid(self.uid)
        self.root_umask = os.umask(self.user_umask)

    def become_root(self):

        os.seteuid(0)
        os.setegid(0)
        os.umask(self.root_umask)

    def become_user(self):

        os.seteuid(0)
        os.setgid(self.gid)
        os.setuid(self.uid)
        os.umask(self.user_umask)

    @contextmanager
    def as_root(self):

        self.become_root()
        try:
            yield
        finally:
            self.revoke_root()

    def obtain_env(self, root_privileges = False, **env):

        if root_privileges:
            base_env = dict(self.root_environment)
        else:
            base_env = dict(self.user_environment)

        base_env.update(env)
        return base_env

    def subproc(self, invoke, command, args = [], root_privileges = False, neutral_language = False, env = None, preexec_fn = None, **options):

        if not path.isabs(command):
            raise Exception('Command %s is not an absolute path.' % command)

        if not path.exists(command):
            raise Exception('Command %s does not exist.' % command)

        process_env = dict(env) if env is not None else self.obtain_env(root_privileges)

        if neutral_language:
            process_env['LANG'] = 'C'
            process_env['LC_ALL'] = 'C'

        set_privileges = (self.become_root if root_privileges else self.become_user) if self.rootable else None

        return invoke(command, args, env = process_env,
          preexec_fn = subproc.join_preexec_fn(preexec_fn, set_privileges),
          **options)

    def communicate(self, *args, **kwargs):

        return self.subproc(subproc.communicate, *args, **kwargs)

    def capture(self, *args, **kwargs):

        return self.subproc(subproc.capture, *args, **kwargs)

    def call(self, *args, **kwargs):

        return self.subproc(subproc.call, *args, **kwargs)

    def ensure_writable_file(self, loc, root_only = False):

        if pathutil.is_writable_file(loc):
            return

        with self.as_root():
            if not path.isfile(loc):
                exclusive.write(loc, '')
            if not root_only:
                os.chown(loc, self.uid, self.gid)

        if not root_only:
            os.chmod(loc, stat.S_IRUSR | stat.S_IWUSR)

    def ensure_accessible_dir(self, loc, root_only = False):

        if pathutil.is_accessible_dir(loc):
            return

        with self.as_root():
            if not path.isdir(loc):
                os.mkdir(loc)
            if not root_only:
                os.chown(loc, self.uid, self.gid)
            else:
                os.chmod(loc, stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR
                  | stat.S_IRGRP | stat.S_IXGRP | stat.S_IROTH | stat.S_IXOTH)

        if not root_only:
            os.chmod(loc, stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR)

    def ensure_no_file(self, loc):

        with self.as_root():
            exclusive.check_and_delete(loc)

    def host_user_log_handler(self, name):

        assert os.sep not in name, 'Name must not be a path: "%s"' % name

        logdir = path.join(host_user_logbase, name)
        logfile = path.join(logdir, self.username + '.log')
        if not pathutil.is_writable_file(logfile):
            self.ensure_accessible_dir(logdir, root_only = True)
            self.ensure_writable_file(logfile)

        return default.common_log_file_handler(logfile)
