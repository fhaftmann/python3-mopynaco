
'''
    Runtime environment of python.
'''

# Copyright 2013 - 2015 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


from contextlib import contextmanager
from os import path
import os
import sys
import inspect
import resource

from . import pathutil


__all__ = ['abspath_of_invocation', 'dispatch_after_invocation_name',
  'suppressing_stderr', 'suppressing_stdout', 'suppress_coredump']


def abspath_of_invocation():

    loc = sys.argv[0]
    if loc == '-c' or loc == '':
        return path.abspath('')
    else:
        return path.abspath(path.dirname(loc))


def dispatch_after_invocation_name(**kwargs):

    invocation_names = list(reversed([(path.basename(loc), path.dirname(loc)) for loc in pathutil.absolute_linkchain(sys.argv[0])]))
    dispatch_invocation_loc = None
    for invocation_name, loc in invocation_names:
        dispatch = kwargs.get(invocation_name, None)
        if dispatch is not None:
            dispatch_invocation_loc = dispatch, loc
            break
    if dispatch_invocation_loc is None:
        raise Exception('Bad invocation name: %s' % list(invocation_names)[0][0])
    dispatch, invocation_loc = dispatch_invocation_loc

    argspec = inspect.getargspec(dispatch)
    expected_min = len(argspec.args) - (len(argspec.defaults) if argspec.defaults is not None else 0)
    expected_max = len(argspec.args) if argspec.varargs is None else None
    args = [invocation_loc] + sys.argv[1:]
    if len(args) < expected_min or expected_max is not None and expected_max < len(args):
        raise Exception('Bad number of arguments')

    return dispatch(*args)


@contextmanager
def suppressing_stderr():

    stderr = sys.stderr
    try:
        with open(os.devnull, 'w') as writer:
            sys.stderr = writer
            yield
    finally:
        sys.stderr = stderr


@contextmanager
def suppressing_stdout():

    stdout = sys.stdout
    try:
        with open(os.devnull, 'w') as writer:
            sys.stdout = writer
            yield
    finally:
        sys.stdout = stdout


@contextmanager
def redirecting_stdout_to_stderr():

    stdout = sys.stdout
    try:
        sys.stdout = sys.stderr
        yield
    finally:
        sys.stdout = stdout


def suppress_coredump():

    resource.setrlimit(resource.RLIMIT_CORE, (0, 0))


def fork_into_background(f, *args, **kwargs):

    pid = os.fork()
    if pid == 0:
        os.close(0)
        os.close(1)
        os.close(2)
        f(*args, **kwargs)
