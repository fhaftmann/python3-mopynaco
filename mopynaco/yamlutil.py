
'''
    Miscellaneous conveniences around yaml, with particular focus
    on formally untyped text-only data.
'''

# Copyright 2013 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['read', 'parse', 'write', 'unparse']


from io import StringIO
import yaml

from . import text


class Quote_Free_Numbers_Dumper(yaml.SafeDumper):

    def process_scalar(self):

        if text.is_number(self.event.value):
            self.event.implicit = True, self.event.implicit[1]
            self.style = '"'
        yaml.SafeDumper.process_scalar(self)


def dump(value, dest):

    return yaml.dump(value, dest, Dumper = Quote_Free_Numbers_Dumper,
      default_flow_style = False, default_style = '')


def parse(txt):

    assert isinstance(txt, str)
    buffer = StringIO(txt)
    result = read(buffer)
    buffer.close()
    return result


def unparse(value):

    return dump(value, None)


def read(reader):

    return yaml.load(reader, Loader = yaml.BaseLoader)


def write(writer, value):

    assert writer is not None
    dump(value, writer)
