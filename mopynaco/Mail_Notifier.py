
'''
    Simple notification by email.
'''

# Copyright 2014 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['Mail_Notifier']


import email.mime.text
import smtplib
import socket

from . import text


class Mail_Notifier(object):

    def __init__(self, smtp, from_name, to_address):

        self.smtp = smtp
        self.from_address = from_name + '@' + socket.gethostname()
        self.to_address = to_address

    def send(self, subject, txt):

        msg = email.mime.text.MIMEText(text.utf8_of(txt), 'plain')
        msg['Subject'] = subject
        msg['From'] = self.from_address
        msg['To'] = self.to_address

        sending = smtplib.SMTP(self.smtp)
        sending.sendmail(self.from_address, [self.to_address],
          msg.as_string())
        sending.quit()
