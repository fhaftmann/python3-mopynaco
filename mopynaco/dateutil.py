
'''
    Miscellaneous date and time utilities.
'''

# Copyright 2011 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['parse_abbrev_month', 'parse_unix_date', 'day_span']


from datetime import datetime, date, time, timedelta

from . import text


c_months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']


def parse_abbrev_month(s):

    try:
        return c_months.index(s) + 1
    except ValueError:
        raise ValueError('Bad month: %s' % s)


def parse_unix_date(s):

    try:
        abbrev_month, raw_day, raw_time, raw_year = s.split()
        year = int(raw_year)
        month = parse_abbrev_month(abbrev_month)
        day = int(raw_day)
        hour, minute, seconds = list(map(int, raw_time.split(':')))
        return datetime(year, month, day, hour, minute, seconds)
    except ValueError:
        raise ValueError('Bad date: %s' % s)


def day_span(d):

    return datetime.combine(d, time.min), datetime.combine(d, time.max)


class german_date(text.value.type):

    content_type = date
    format = '%d.%m.%Y'

    def read(self, txt):

        assert isinstance(txt, str)
        return datetime.strptime(txt, self.format).date()

    def write(self, value):

        assert isinstance(value, self.content_type)
        return value.strftime(self.format)


class german_clocktime(text.value.type):

    content_type = time
    format = '%H:%M'

    def read(self, txt):

        assert isinstance(txt, str)
        return datetime.strptime(txt, self.format).time()

    def write(self, value):

        assert isinstance(value, self.content_type)
        return value.strftime(self.format)
