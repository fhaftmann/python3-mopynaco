
'''
    Miscellaneous file and path utilities.
'''

# Copyright 2015 - 2017 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['list_dir', 'list_tree', 'list_symlinks_in_tree',
  'explode', 'implode', 'is_prefix_of', 'combine', 'absolute_linkchain', 'split_symlink',
  'is_readable_file', 'is_writable_file', 'is_executable_file', 'is_readable_dir', 'is_accessible_dir', 'ensure_accessible_dir',
  'purge', 'purge_content', 'remove_if_symlink', 'redirect_symlink',
  'get_read_parts', 'get_exec_parts', 'get_dir_parts', 'ensure_trailing_sep', 'home_directory', 'expand_user_home',
  'temporary_directory', 'atomicity_guard', 'file_lock']


from contextlib import contextmanager
import re
from pathlib import PurePath
from os import path
import fnmatch
import os
import fcntl
import shutil
import stat
import getpass
import tempfile

from . import seq


def path_as_string(p):

    assert isinstance(p, (str, PurePath))

    return str(p)


def list_dir(loc, strict_root = False):

    loc = path_as_string(loc)

    if strict_root or path.isdir(loc):
        for name in os.listdir(loc):
            yield path.join(loc, name), name


def list_tree(loc, topdown = True, onerror = None, followlinks = False, strict_root = False):

    loc = path_as_string(loc)

    loc_fragments = explode(loc)
    if strict_root or path.isdir(loc):
        for loc_rel, _, names_file in os.walk(loc, topdown, onerror, followlinks):
            _, _, prefix_rel = seq.common_prefix_suffixes(loc_fragments, explode(loc_rel))
            file_entries = [(path.join(loc_rel, name), name) for name in names_file]
            yield (loc_rel, prefix_rel), file_entries


def list_symlinks_in_tree(loc, topdown = True, onerror = None, strict_root = False):

    loc = path_as_string(loc)

    loc_fragments = explode(loc)
    if strict_root or path.isdir(loc):
        for loc_rel, names_dirs, names_file in os.walk(loc, topdown, onerror):
            _, _, prefix_rel = seq.common_prefix_suffixes(loc_fragments, explode(loc_rel))
            entries = [(path.join(loc_rel, name), name) for name in names_dirs + names_file
              if path.islink(path.join(loc, loc_rel, name))]
            yield (loc_rel, prefix_rel), entries


def explode(loc, split = path.split):

    loc = path_as_string(loc)

    if loc == '':
        return ()

    parts = []
    loc_old = None
    while loc != loc_old:
        loc_old = loc
        loc, base = split(loc_old)
        if base != '':
            parts.append(base)
    if loc != '' and all(c == os.sep for c in loc): ## consider absolute paths
        parts.append(os.sep)

    return tuple(reversed(parts))


def implode(parts, join = path.join):

    return join(*parts) if parts else ''


def is_prefix_of(loc1, loc2):

    return seq.is_prefix_of(explode(loc1), explode(loc2))


def combine(loc1, loc2):

    parts1 = explode(loc1)
    parts2 = explode(loc2)

    prefix, suffix1, suffix2 = seq.common_prefix_suffixes(parts1, parts2)

    return implode(prefix), implode(suffix1), implode(suffix2)


def absolute_linkchain(loc):

    loc = path_as_string(loc)

    loc = path.abspath(loc)
    chain = [loc]
    while path.islink(loc):
        target = os.readlink(loc)
        loc = path.abspath(path.join(path.dirname(loc), target))
        if loc in chain:
            raise IOError('Circular link chain at %s' % loc)
        chain.append(loc)
    return chain


def split_symlink(loc):

    parts = explode(loc)
    for i in range(len(parts)):
        prefix = implode(parts[:i + 1])
        suffix = implode(parts[i + 1:])
        if path.islink(prefix):
            return prefix, suffix
    return None


def is_readable_file(loc):

    loc = path_as_string(loc)

    return path.isfile(loc) and os.access(loc, os.R_OK)


def is_writable_file(loc):

    loc = path_as_string(loc)

    return path.isfile(loc) and os.access(loc, os.R_OK | os.W_OK)


def is_executable_file(loc):

    loc = path_as_string(loc)

    return path.isfile(loc) and os.access(loc, os.R_OK | os.X_OK)


def is_readable_dir(loc):

    loc = path_as_string(loc)

    return path.isdir(loc) and os.access(loc, os.R_OK | os.X_OK)


def is_accessible_dir(loc):

    loc = path_as_string(loc)

    return path.isdir(loc) and os.access(loc, os.R_OK | os.W_OK | os.X_OK)


def ensure_accessible_dir(loc, starting_from = None):

    loc = path_as_string(loc)

    if is_accessible_dir(loc):
        return
    if not path.isdir(loc):
        if starting_from is not None:
            parent = path.dirname(loc)
            if is_prefix_of(starting_from, parent):
                ensure_accessible_dir(parent, starting_from)
        os.mkdir(loc)
    os.chmod(loc, stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR)


def purge(loc, permissive = False):

    loc = path_as_string(loc)

    if not path.islink(loc) and path.isdir(loc):
        shutil.rmtree(loc)
    elif not permissive or path.lexists(loc):
        os.remove(loc)


def purge_content(loc):

    loc = path_as_string(loc)

    for sub_loc, _ in list_dir(loc):
        purge(sub_loc)


def remove_if_symlink(loc):

    loc = path_as_string(loc)

    if path.islink(loc):
        os.unlink(loc)


def redirect_symlink(target, loc):

    loc = path_as_string(loc)

    remove_if_symlink(loc)
    if path.exists(loc):
        raise IOError('File exists: "%s"' % loc)
    os.symlink(target, loc)


def duplicate(src, dst):

    src = path_as_string(src)
    dst = path_as_string(dst)

    if not path.isdir(dst):
        raise OSError('No such directory: "%s"' % dst)

    if path.islink(src):
        target = os.readlink(src)
        loc_symlink = path.join(dst, path.basename(src))
        if path.lexists(loc_symlink):
            purge(loc_symlink)
        os.symlink(target, loc_symlink)
    elif path.isdir(src):
        dst_dir = path.join(dst, path.basename(src))
        if not path.isdir(dst_dir):
            os.mkdir(dst_dir)
            shutil.copystat(src, dst_dir)
        for loc, _ in list_dir(src):
            duplicate(loc, dst_dir)
    else:
        shutil.copy2(src, dst)


def replicate(src, leafs, dst, ignore_nonexisting = False):

    src = path_as_string(src)
    dst = path_as_string(dst)

    for leaf in leafs:
        assert not path.isabs(leaf), 'Leaf must not be an absolute path: "%s"' % leaf
        assert leaf != '', 'Leaf must not be empty'
        assert os.curdir not in explode(leaf), 'Leaf must not contain "%s": "%s"' % (os.curdir, leaf)
        assert os.pardir not in explode(leaf), 'Leaf must not contain "%s": "%s"' % (os.pardir, leaf)

    for leaf in leafs:
        loc_src = path.join(src, leaf)
        if not ignore_nonexisting or path.lexists(loc_src):
            loc_dst = path.join(dst, path.dirname(leaf))
            if not path.isdir(loc_dst):
                os.makedirs(loc_dst)
            duplicate(loc_src, loc_dst)


def get_read_parts(loc, pat = None):

    loc = path_as_string(loc)

    return sorted([file_loc for file_loc, file_name in list_dir(loc, pat)
      if (pat is None or fnmatch.fnmatch(file_name, pat)) and is_readable_file(file_loc)])


def get_exec_parts(loc):

    loc = path_as_string(loc)

    return sorted([file_loc for file_loc, _ in list_dir(loc)
      if is_executable_file(file_loc)])


def get_dir_parts(loc):

    loc = path_as_string(loc)

    return sorted([file_loc for file_loc, _ in list_dir(loc)
      if is_readable_dir(file_loc)])


def ensure_trailing_sep(loc):

    loc = path_as_string(loc)

    if loc == '':
        return os.curdir + os.sep
    elif loc[-1] != os.sep:
        return loc + os.sep
    else:
        return loc


def home_directory(user = None):

    assert user is None or os.sep not in user

    username = user if user is not None else getpass.getuser()
    loc_tilde = '~' + username
    home_candidate = path.expanduser(loc_tilde)
    return None if loc_tilde == home_candidate else home_candidate


percent_anchor = re.compile(r'%(u|h|%)')

def expand_user_home(loc, user = None):

    loc = path_as_string(loc)

    username = user if user is not None else getpass.getuser()
    userhome = home_directory(username)

    def repl(match):
        c = match.group(1)
        if c == 'u':
            return username
        elif c == 'h':
            return userhome
        elif c == '%':
            return '%'

    return percent_anchor.sub(repl, loc)

@contextmanager
def temporary_directory(suffix = '', prefix = 'tmp', dir = None):

    def permissive_rmdir(loc):
        try:
            os.rmdir(loc)
        except:
            pass

    dir = path_as_string(dir) if dir is not None else None

    loc = tempfile.mkdtemp(suffix, prefix, dir)
    try:
        yield loc
    except:
        permissive_rmdir(loc)
        raise
    else:
        os.rmdir(loc)


@contextmanager
def atomicity_guard(loc, suffix = '.emerging'):

    def permissive_rm(loc):
        try:
            os.remove(loc)
        except:
            pass

    loc = path_as_string(loc)

    emerging = loc + suffix
    try:
        yield emerging
        if path.exists(emerging):
            os.rename(emerging, loc) ## atomicity asserted by POSIX
    except:
        permissive_rm(emerging)
        raise


@contextmanager
def file_lock(loc, make_dir = True):

    loc = path_as_string(loc)

    parent = path.dirname(loc)
    if not path.isdir(parent) and make_dir:
        os.makedirs(parent)
    writer = open(loc, 'w')
    fcntl.lockf(writer, fcntl.LOCK_EX)
    try:
        yield
    finally:
        try:
            os.remove(loc)
        except:
            pass
        writer.close()
