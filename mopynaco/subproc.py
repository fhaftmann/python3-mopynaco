
'''
    Subprocess communication based on subprocess.Popen
    with transparent unicode/string conversions,
    safe iterable pipes and optional logging.
'''

# Copyright 2013 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['Output', 'reader_pipe', 'writer_pipe', 'communicate', 'capture',
  'call', 'safe_shellarg', 'join_preexec_fn', 'Called_Process_Error']


import contextlib
from queue import Queue
from os import path
import io
import threading
import time
import logging
import subprocess
import shlex
import errno
import os

from . import seq
from .Enum import Enum
from . import text


# auxiliary

buffer_size = 1024
default_log_name_length = 20


class Background_Reader(threading.Thread):

    def __init__(self, reader, marker, queue):

        threading.Thread.__init__(self)
        self.daemon = True

        self.reader = reader
        self.marker = marker
        self.queue = queue

    def run(self):

        try:
            while True:
                line = self.reader.readline()
                if line == '':
                    break
                self.queue.put((self.marker, line.rstrip('\n')))
                time.sleep(0) ## hand over remaining time slice to other threads
        finally:
            self.reader.close()
            self.queue.put(None)


class Background_Sponge(threading.Thread):

    def __init__(self, fd):

        threading.Thread.__init__(self)
        self.daemon = True

        self.fd = fd
        self.blocks = []

    def run(self):

        try:
            while True:
                data = os.read(self.fd, buffer_size)
                if data == b'':
                    break
                self.blocks.append(data)
        finally:
            os.close(self.fd)

    def content(self):

        self.join()
        return b''.join(self.blocks)


class Background_Sprinkler(threading.Thread):

    def __init__(self, fd, data):

        threading.Thread.__init__(self)
        self.daemon = True

        self.fd = fd
        self.datas = seq.slices(data, buffer_size)

    def run(self):

        try:
            for data in self.datas:
                try:
                    os.write(self.fd, data)
                except OSError as e:
                    if e.errno != errno.EPIPE:
                        raise
        finally:
            os.close(self.fd)


def safe_shellarg(s):

    return shlex.quote(s)


# subprocess control

def join_preexec_fn(preexec_fn1, preexec_fn2):

    return preexec_fn1 if preexec_fn2 is None \
      else preexec_fn2 if preexec_fn1 is None \
      else lambda: (preexec_fn1(), preexec_fn2())[1]


class Bad_Executable(Exception): pass

class Called_Process_Error(subprocess.CalledProcessError):

    def __init__(self, return_code, cmd, output = None, accepted_return_codes = (0,)):

        subprocess.CalledProcessError.__init__(self, return_code, cmd, output)
        self.accepted_return_codes = accepted_return_codes

    def __str__(self):

        return "Command '{}' returned exit status {}; accepted return code(s) include {}" \
          .format(self.cmd, self.returncode, ', '.join([str(k) for k in self.accepted_return_codes]))

class Output(Enum('stdout', 'stderr')): pass

class Communication(object):

    def __init__(self, proc, proc_args = [], input = None, stdin_errors = text.replace,
      stdout_pipe = True, stdout_errors = text.replace,
      stderr_pipe = True, stderr_errors = text.replace,
      log_callback = None, log_name_length = default_log_name_length, log_verbose = True,
      working_directory = None, env = None, more_env = {},
      pass_fds = [], preexec_fn = None, accepted_return_codes = None, **kwargs):

        assert input is None or isinstance(input, str)

        self.all_proc_args = [proc] + proc_args

        self.log_callback = log_callback
        filename = path.basename(self.all_proc_args[0])
        self.log_shortname = filename if log_name_length is None \
          else text.abbreviate(filename, log_name_length)
        self.log_verbose = log_verbose
        self.reading_started = False
        self.return_code = None
        self.accepted_return_codes = accepted_return_codes

        env = dict(os.environ) if env is None else dict(env)
        env.update(more_env)
        for key in list(env):
            if env[key] is None:
                del env[key]

        close_fds, pass_fds = \
          (False, []) if pass_fds is Ellipsis else (True, pass_fds)

        try:
            self.subproc = subprocess.Popen(self.all_proc_args,
              stdin = None if input is None else subprocess.PIPE,
              stdout = subprocess.PIPE, stderr = subprocess.PIPE,
              cwd = working_directory, env = env,
              close_fds = close_fds, pass_fds = pass_fds,
              preexec_fn = preexec_fn, **kwargs)
        except OSError as e:
            if e.errno not in (errno.ENOENT, errno.EACCES, errno.ENOEXEC):
                raise
            else:
                raise Bad_Executable(proc, e.strerror, e.errno)

        self.input = None if input is None else input.encode(text.utf8, stdin_errors)
        self.selected_markers = []
        self.stdout = io.TextIOWrapper(self.subproc.stdout, encoding = text.utf8,
          errors = stdout_errors, line_buffering = True)
        if stdout_pipe:
            self.selected_markers.append(Output.stdout)
        self.stderr = io.TextIOWrapper(self.subproc.stderr, encoding = text.utf8,
          errors = stderr_errors, line_buffering = True)
        if stderr_pipe:
            self.selected_markers.append(Output.stderr)

        if log_callback is not None:
            self.log_callback('[%s invoke] ' % self.log_shortname +
              ' '.join([safe_shellarg(proc_arg) for proc_arg in self.all_proc_args]))

    def __iter__(self):

        assert not self.reading_started, 'Attempt to repeat iteration over process output'
        self.reading_started = True

        queue = Queue()
        stdout_reader = Background_Reader(self.stdout, Output.stdout, queue)
        stdout_reader.start()
        stderr_reader = Background_Reader(self.stderr, Output.stderr, queue)
        stderr_reader.start()
        active_readers = 2

        if self.input is not None:
            self.subproc.stdin.write(self.input)
            self.subproc.stdin.close()

        while active_readers > 0:
            entry = queue.get()
            if entry is None:
                active_readers -= 1
            else:
                marker, line = entry
                if self.log_callback is not None and (self.log_verbose or marker is Output.stderr):
                    self.log_callback('[%s %s] %s' %
                      (self.log_shortname, 'stdout' if marker is Output.stdout else 'stderr', line))
                if marker in self.selected_markers:
                    yield entry

        stdout_reader.join()
        stderr_reader.join()

        self.return_code = self.subproc.wait()

        if self.log_callback is not None:
            self.log_callback('[%s return] %i' % (self.log_shortname, self.return_code))

    def terminate(self):

        self.subproc.terminate()
        self.selected_markers = []

    def kill(self):

        self.subproc.kill()
        self.selected_markers = []

    def wait(self):

        if not self.reading_started:
            for _ in self: pass

        assert self.return_code is not None, 'Attempt to wait for process with partially processed output'

        if self.accepted_return_codes is not None:
            if self.return_code not in self.accepted_return_codes:
                raise Called_Process_Error(self.return_code, self.all_proc_args,
                  self.accepted_return_codes)

        return self.return_code


# simplistic logging

def simple_logger():

    handler = logging.StreamHandler()
    log = logging.getLogger()
    log.addHandler(handler)
    log.setLevel(logging.DEBUG)

    return log.debug


# congestion-free pipes, particularly for external processes

@contextlib.contextmanager
def reader_pipe(content):

    fd_read, fd_write = os.pipe()
    os.set_inheritable(fd_read, True)
    pipe_writer = Background_Sprinkler(fd_write, content.encode(text.utf8))
    pipe_writer.start()
    try:
        yield fd_read
    finally:
        os.close(fd_read)
        pipe_writer.join()


class Writer_Pipe(object):

    def __init__(self):

        self.fd_read, self.fd_write = os.pipe()
        os.set_inheritable(self.fd_write, True)
        self.content = None

    @contextlib.contextmanager
    def fd(self):

        assert self.content is None, 'Attempt to re-obtain file descriptor of writer pipe'
        pipe_reader = Background_Sponge(self.fd_read)
        pipe_reader.start()
        try:
            yield self.fd_write
        finally:
            os.close(self.fd_write)
            binary_content = pipe_reader.content()
            self.content = str(binary_content, encoding = text.utf8)


@contextlib.contextmanager
def writer_pipe():

    writer_pipe = Writer_Pipe()
    try:
        yield writer_pipe
    finally:
        if writer_pipe.content is None:
            with writer_pipe.fd() as _:
                pass


# subprocess user interfaces

@contextlib.contextmanager
def communicate(*args, **kwargs):

    communication = Communication(*args, **kwargs)
    yield communication
    communication.wait()


def capture(*args, **kwargs):

    communication = Communication(*args, stdout_pipe = True, stderr_pipe = True, **kwargs)
    stdout_lines, stderr_lines = [], []
    for marker, line in communication:
        if marker is Output.stdout:
            stdout_lines.append(line)
        elif marker is Output.stderr:
            stderr_lines.append(line)
    return_code = communication.wait()

    return '\n'.join(stdout_lines), '\n'.join(stderr_lines), return_code


def call(*args, **kwargs):

    communication = Communication(*args, stdout_pipe = False, stderr_pipe = False, **kwargs)
    return_code = communication.wait()

    return return_code
