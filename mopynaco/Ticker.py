
'''
    Thread-safe singleton value ticker with latency.
'''

# Copyright 2013 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['Ticker']


from threading import Condition, Event, Lock


class Ticker(object):

    """Singleton value ticker safe for one producer and one consumer.
    Existing value is discarded when a new value is put.
    Putting None finishes tickering."""

    def __init__(self):

        self.available = Condition(Lock())
        self.finished = Event()
        self.value = None

    def get(self, latency):

        if self.finished.wait(latency):
            return None
        else:
            with self.available:
                if self.finished.is_set():
                    return None
                else:
                    self.available.wait()
                    x = self.value
                    return x

    def put(self, x):

        if not self.finished.is_set():
            with self.available:
                if x is None:
                    self.finished.set()
                self.value = x
                self.available.notify()
