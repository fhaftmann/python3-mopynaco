
'''
    Convenience wrapper around ConfigParser.
'''

# Copyright 2016 Technische Universität München

# This software may be used and distributed according to the terms of the
# GNU General Public License version 2 or any later version.
# You may obtain a copy of the GNU General Public License at:

#   http://www.gnu.org/licenses/gpl-2.0.html


__all__ = ['Ini_Parser']


from configparser import RawConfigParser


class Ini_Parser(RawConfigParser):

    optionxform = str
