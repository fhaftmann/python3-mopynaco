
'''
    Indexed enumeration values.
'''

# Copyright 2010 Technische Universität München

# This software may be used and distributed according to the terms of the
# GNU General Public License version 2 or any later version.
# You may obtain a copy of the GNU General Public License at:

#   http://www.gnu.org/licenses/gpl-2.0.html


__all__ = ['Enum']


def Enum(*enums):

    class __metaclass__(type):

        def __new__(Meta_Class, name, bases, dict):

            Class = type.__new__(Meta_Class, name, bases, dict)
            values = [Class(name, enum) for enum in enums]
            for enum, value in zip(enums, values):
                setattr(Class, enum, value)
            Class.__values__ = values
            return Class

        def __getitem__(Class, index):

            return Class.__values__[index]

        def __len__(Class):

            return len(Class.__values__)

        def __iter__(Class):

            for value in Class.__values__:
                yield value

    class Enum(str, metaclass = __metaclass__):

        def __new__(Class, name, enum):

            value = str.__new__(Class, name + '.' + enum)
            value.enum = enum
            return value

    return Enum
