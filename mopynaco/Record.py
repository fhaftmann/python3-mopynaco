
'''
    Record types as syntactic sugar for collections.namedtuple.
'''

# Copyright 2013 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['Record']


from collections import namedtuple


def Record(*fields):

    class __metaclass__(type):

        def __new__(Meta_Class, name, bases, dict):

            if name == 'Record' and bases == (object,):
                return type.__new__(Meta_Class, name, bases, dict)
            else:
                Class = namedtuple(name, fields)
                @property
                def __mapping__(self):
                    for field in fields:
                        yield field, getattr(self, field)
                Class.__mapping__ = __mapping__
                for name in dict:
                    if name == '__doc__':
                        continue
                    setattr(Class, name, dict[name])
                return Class

    class Record(object, metaclass = __metaclass__): pass

    return Record
