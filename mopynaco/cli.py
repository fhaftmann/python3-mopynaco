
'''
    Functionality for command line interfaces.
'''

# Copyright 2013 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['line_input', 'key_input']


from threading import Thread, Event
import tty
import termios
import sys


def line_input(prompt = '', timeout = None, accept = None):

    evt = Event()
    answer = [None] # singleton mutable variable

    def _input():

        try:
            answer[0] = input(prompt)
        except EOFError:
            answer[0] = ''
        finally:
            evt.set()

    while True:
        thread = Thread(target = _input)
        thread.daemon = True
        thread.start()
        evt.wait(timeout)
        result = answer[0]
        if result is None:
            print()
            return None
        elif accept is None or accept(result):
            return result
        else:
            thread.join()
            evt.clear()
            timeout = None


def key_input():

    stdin_fileno = sys.stdin.fileno()
    terminal_settings = termios.tcgetattr(stdin_fileno)

    try:
        tty.setcbreak(stdin_fileno)
        answer = sys.stdin.read(1)
    finally:
        termios.tcsetattr(stdin_fileno, termios.TCSADRAIN, terminal_settings)

    return answer
