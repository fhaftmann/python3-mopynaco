
'''
    Singleton classes.
    Can be instantiated only once.
'''

# Copyright 2010 Technische Universität München

# This software may be used and distributed according to the terms of the
# GNU General Public License version 2 or any later version.
# You may obtain a copy of the GNU General Public License at:

#   http://www.gnu.org/licenses/gpl-2.0.html


__all__ = ['Multiple_Instantiation', 'Singleton']


class Multiple_Instantiation(TypeError):

    pass


class Singleton(object):

    instantiated = False

    def __new__(Class, *args, **kwargs):

        if Class.instantiated:
            raise Multiple_Instantiation(Class)
        else:
            Class.instantiated = True
            return object.__new__(Class)
