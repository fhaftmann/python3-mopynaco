
'''
    Various default pragmatics.
'''

# Copyright 2011 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['datetime_format', 'datetime_format_microsecond',
  're_text_datetime_format', 're_text_datetime_format_microsecond', 'common_log_file_handler']


import logging


datetime_format = '%Y-%m-%d %H:%M:%S'

datetime_format_microsecond = datetime_format + ',%f'

re_text_datetime_format = R'\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}'
re_text_datetime_format_microsecond = R'\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2},\d+'


def common_log_file_handler(logfile):

    handler = logging.FileHandler(logfile)
    handler.setFormatter(logging.Formatter('%(asctime)s %(levelname)s %(message)s'))

    return handler
