
'''
    Miscellaneous conveniences around xml, based on xml etree.
'''

# Copyright 2015 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['format_qname', 'read_doc_permissive', 'write_doc',
  'indent_elem', 'indent', 'indent_file']


from lxml import etree


def format_qname(qname):

    return '{' + qname[0] + '}' + qname[1]


def read_doc_permissive(src):

    try:
        doc = etree.parse(src)
    except etree.XMLSyntaxError:
        return None
    return doc


def write_doc(dst, doc):

    doc.write(dst, encoding = doc.docinfo.encoding,
      standalone = doc.docinfo.standalone, xml_declaration = True)


def indent_elem(elem, mixed_content = [], indent = 2):

    tags_content = [format_qname(qname) for qname in mixed_content]

    def indent_(level, elem):

        if len(elem) > 0 and elem.tag not in tags_content:
            sub_level = level + 1
            sub_indent = '\n' + (' ' * indent * sub_level)
            elem.text = sub_indent
            for sub_elem in elem[:-1]:
                sub_elem.tail = sub_indent
                indent_(sub_level, sub_elem)
            elem[-1].tail = '\n' + (' ' * indent * level)
            indent_(sub_level, elem[-1])

    indent_(0, elem)


def indent(reader, writer, mixed_content = [], indent = 2):

    doc = etree.parse(reader)
    indent_elem(doc.getroot(), mixed_content, indent)
    write_doc(writer, doc)


def indent_file(loc, mixed_content = [], indent = 2):

    doc = etree.parse(loc)
    indent_elem(doc.getroot(), mixed_content, indent)
    with open(loc, 'wb') as writer:
        write_doc(writer, doc)
