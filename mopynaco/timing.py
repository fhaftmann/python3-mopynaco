
'''
    Various timing facilities.
'''

# Copyright 2013 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['sleep', 'retry']


import time


def sleep(duration):

    if duration > 0:
        time.sleep(duration)


def retry(at_most_all, at_most_between):

    do_until = time.time() + at_most_all
    k = 0
    while time.time() < do_until:
        start_iter = time.time()
        yield k
        sleep(at_most_between(k) - start_iter + time.time())
        k += 1
