
'''
    Structural sum types with projections and unpacking to almost-everywhere-none tuples.
'''

# Copyright 2013 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['Sum']


def Sum(*selectors):

    class __metaclass__(type):

        def __new__(Meta_Class, name, bases, props):

            Class = type.__new__(Meta_Class, name, bases, props)
            Class.__typename__ = name
            Class.__selectors__ = list(selectors)
            return Class

    class Sum(object, metaclass = __metaclass__):

        def __init__(self, **kwargs):

            if len(kwargs) != 1:
                raise ValueError('Sum type {0} takes exactly one selector argument'.format(type(self)))
            key, value = kwargs.popitem()
            if key not in self.__selectors__:
                raise ValueError('No such selector in sum type {0}: {1}'.format(type(self), key))
            if value is None:
                raise ValueError('None-type not allowed as component of sum type')

            for selector in self.__selectors__:
                setattr(self, selector, value if selector == key else None)

        def __str__(self):

            for selector in self.__selectors__:
                value = getattr(self, selector, None)
                if value is not None:
                    return type(self).__typename__ + '.' + selector + '(' + str(value) + ')'

        def __repr__(self):

            for selector in self.__selectors__:
                value = getattr(self, selector, None)
                if value is not None:
                    return type(self).__typename__ + '.' + selector + '(' + repr(value) + ')'

        def __iter__(self):

            for selector in self.__selectors__:
                yield getattr(self, selector, None)

        def __eq__(self, other):

            return isinstance(other, type(self)) and \
              all(getattr(self, selector, None) == getattr(other, selector, None) for selector in self.__selectors__)

        def __hash__(self):

            return hash(tuple(getattr(self, selector, None) for selector in self.__selectors__))

    return Sum
