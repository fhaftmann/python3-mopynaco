
'''
    Various bash facilities.
'''

# Copyright 2017 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['get_value_from', 'get_array_from']


from os import path
import subprocess
import shlex

from mopynaco import text


def output(cmd):

    return subprocess.check_output(['bash', '-c', '\n'.join(cmd)],
      encoding = text.utf8)


def get_value_from(loc, name):

    if not path.exists(loc):
        raise IOError('File not found: {}'.format(loc))

    cmd = ['source ' + shlex.quote(loc),
      'echo -n "${' + shlex.quote(name) + '}"']

    return output(cmd)


def get_array_from(loc, name):

    if not path.exists(loc):
        raise IOError('File not found: {}'.format(loc))

    cmd = ['source ' + shlex.quote(loc),
      'for VALUE in "${' + shlex.quote(name) + '[@]}"',
      '  do echo "${VALUE}"',
      'done']

    return output(cmd).split('\n')[:-1]
