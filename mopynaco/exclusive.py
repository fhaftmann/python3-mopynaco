
'''
    Exclusive read/write/delete-operations on files.

    Operative on certain UNIXes only (including Linux).
'''

# Copyright 2011 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.



__all__ = ['read', 'write', 'atomic_write', 'atomic_modification',
  'check_and_write', 'read_or_write', 'check_and_delete']


from contextlib import contextmanager
from os import path
import os
import fcntl
import errno

from mopynaco import text


read_length = 1024
file_mode = 0o644 ## nb. octal literal


def perhaps_open(loc, flags):

    try:
        fd = os.open(loc, flags)
    except OSError as e:
        if e.errno != errno.ENOENT:
            raise
        else:
            return None
    return fd


def content_of(fd):

    content = []
    while True:
        bulk = os.read(fd, read_length)
        if bulk == b'':
            break
        content.append(bulk)
    return text.of_utf8(b''.join(content))


def read(loc):

    fd = perhaps_open(loc, os.O_RDONLY | os.O_NOFOLLOW)
    if fd is None:
        return None
    fcntl.lockf(fd, fcntl.LOCK_SH)
    content = content_of(fd)
    fcntl.lockf(fd, fcntl.LOCK_UN)
    os.close(fd)
    return content


def write(loc, content):

    fd = os.open(loc, os.O_WRONLY | os.O_CREAT | os.O_TRUNC | os.O_NOFOLLOW, file_mode)
    fcntl.lockf(fd, fcntl.LOCK_EX)
    os.write(fd, text.utf8_of(content))
    os.fdatasync(fd)
    fcntl.lockf(fd, fcntl.LOCK_UN)
    os.close(fd)


def mk_tmp_loc(loc, name_tmp):

    assert path.basename(name_tmp) == name_tmp, \
      'Filename must not be a path: "%s"' % name_tmp
    assert path.basename(name_tmp) != path.basename(loc), \
      'Filenames must be different: "%s" vs. "%s"' % (loc, name_tmp)

    return path.join(path.dirname(loc), name_tmp)


def atomic_write(loc, name_tmp, content):

    loc_tmp = mk_tmp_loc(loc, name_tmp)
    write(loc_tmp, content)
    os.rename(loc_tmp, loc) ## atomicity asserted by POSIX


@contextmanager
def atomic_modification(loc, name_tmp):

    loc_tmp = mk_tmp_loc(loc, name_tmp)

    try:
        with open(loc) as reader:
            with open(loc_tmp, 'w') as writer:
                yield reader, writer
            os.rename(loc_tmp, loc) ## atomicity asserted by POSIX
    except BaseException:
        check_and_delete(loc_tmp)
        raise


def check_and_write(loc, new_content):

    fd = os.open(loc, os.O_RDWR | os.O_CREAT | os.O_NOFOLLOW, file_mode)
    fcntl.lockf(fd, fcntl.LOCK_EX)
    is_empty = os.read(fd, 1) == b''
    if is_empty:
        os.write(fd, text.utf8_of(new_content))
        os.fdatasync(fd)
    fcntl.lockf(fd, fcntl.LOCK_UN)
    os.close(fd)
    return is_empty


def read_or_write(loc, new_content, may_write = lambda s: s == ''):

    fd = os.open(loc, os.O_RDWR | os.O_CREAT | os.O_NOFOLLOW, file_mode)
    fcntl.lockf(fd, fcntl.LOCK_EX)

    old_content = content_of(fd)

    do_write = may_write(old_content)
    if do_write:
        os.lseek(fd, 0, os.SEEK_SET)
        os.ftruncate(fd, 0)
        os.write(fd, text.utf8_of(new_content))
        os.fdatasync(fd)

    fcntl.lockf(fd, fcntl.LOCK_UN)
    os.close(fd)

    return None if do_write else old_content


def check_and_delete(loc):

    try:
        os.unlink(loc)
    except OSError as e:
        if e.errno != errno.ENOENT:
            raise
        else:
            return False
    return True
