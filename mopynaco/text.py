
'''
    Operations on unicode text, particulary strict unicode / byte stream conversions.
'''

# Copyright 2012 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['utf8', 'strict', 'replace', 'ignore', 'xmlcharrefreplace', 'backslashreplace',
  'of_ascii', 'ascii_of', 'of_utf8', 'utf8_of', 'of_sloppy', 'sloppy_ascii_of', 'sloppy_utf8_of', 'of_anything',
  'is_number', 'int_of',
  'bin_of_base64', 'base64_of_bin',
  'reading_utf8', 'writing_utf8', 'appending_utf8', 'content_utf8_of',
  'of_filename', 'filename_of',
  'subst', 'abbreviate',
  'value', 'subset_value', 'text', 'boolean', 'integer', 'floating_point', 'natural']


import re
import numbers
from os import path
import io
import base64

from .instance import instance
from .exception import masking_value_error


# symbolic identifiers

utf8 = 'utf-8'
strict = 'strict'
replace = 'replace'
ignore = 'ignore'
xmlcharrefreplace = 'xmlcharrefreplace'
backslashreplace = 'backslashreplace'


# conversions

def of_ascii(s):

    assert isinstance(s, bytes)
    return str(s, 'us-ascii', strict)


def ascii_of(txt):

    assert isinstance(txt, str)
    return txt.encode('us-ascii', strict)


def of_utf8(s):

    assert isinstance(s, bytes)
    return str(s, utf8, strict)


def utf8_of(txt):

    assert isinstance(txt, str)
    return txt.encode(utf8, strict)


def of_sloppy(s):

    assert isinstance(s, bytes)
    return str(s, utf8, replace)


def sloppy_ascii_of(txt):

    assert isinstance(txt, str)
    return txt.encode('us-ascii', replace)


def sloppy_utf8_of(txt):

    assert isinstance(txt, str)
    return txt.encode(utf8, replace)


def of_anything(x):

    if isinstance(x, str):
        return x
    elif isinstance(x, bytes):
        return of_sloppy(x)
    else:
        return str(x)


# numbers as strings

def is_number(s):

    assert isinstance(s, str)

    try:
        float(s)
        return True
    except ValueError:
        return False


def int_of(s, default = None, base = 10):

    assert isinstance(s, str)

    try:
        return int(s, base)
    except ValueError:
        return default


# base64-encoded binary data

def base64_of_bin(bin):

    assert isinstance(bin, bytes)
    return of_ascii(base64.b64encode(bin))


def bin_of_base64(txt):

    assert isinstance(txt, str)
    return base64.b64decode(ascii_of(txt))


# file operations

def reading_utf8(loc, buffering = 1):

    return io.open(loc, 'r',
      encoding = utf8, errors = strict, buffering = buffering)


def writing_utf8(loc, buffering = 1):

    return io.open(loc, 'w',
      encoding = utf8, errors = strict, buffering = buffering)


def appending_utf8(loc, buffering = 1):

    return io.open(loc, 'a',
      encoding = utf8, errors = strict, buffering = buffering)


def content_utf8_of(loc, permissive = False):

    if not path.exists(loc) and permissive:
        return None
    else:
        with reading_utf8(loc) as reader:
            content = reader.read()
        return content


# filename conversions -- identity in Python 3!

def of_filename(txt):

    assert isinstance(txt, str)
    return txt


def filename_of(txt):

    assert isinstance(txt, str)
    return txt


# simultaneous substitution on disjoint strings

def subst(mapping):

    assert all(isinstance(s, str) for s in mapping.keys())
    assert all(isinstance(s, str) for s in mapping.values())

    re_keys = re.compile('(' + '|'.join([re.escape(key) for key in mapping.keys()]) + ')')

    def sub(s):
        assert isinstance(s, str)
        return re_keys.sub(lambda match: mapping[match.group()], s)

    return sub


# abbreviating

def abbreviate(txt, max_length, trailing = '…'):

    assert isinstance(txt, str)
    assert max_length > 0
    return txt if len(txt) <= max_length else txt[:max_length - 1] + trailing


# conversions from and to values with subset types -- a simple type system for atomic values

class value(instance.type):

    content_type = type(None)

    def read(self, txt):

        assert isinstance(txt, str)
        return None

    def read_utf8(self, s):

        return self.read(of_utf8(s))

    def write(self, value):

        assert isinstance(value, self.content_type)
        return ''


class subset_value(value.type):

    super_value = value

    @property
    def content_type(self):

        return self.super_value.content_type

    def check(self, value):

        return value

    def read(self, txt):

        return self.check(self.super_value.read(txt))

    def write(self, value):

        return self.super_value.write(self.check(value))


class text(value.type):

    content_type = str

    def read(self, txt):

        assert isinstance(txt, str)
        return txt

    def write(self, value):

        assert isinstance(value, self.content_type)
        return value


class boolean(value.type):

    content_type = bool

    def read(self, txt):

        assert isinstance(txt, str)
        return txt not in ('', 'false', 'False', 'no')

    def write(self, value):

        assert isinstance(value, self.content_type)
        return str(value)


class integer(value.type):

    content_type = numbers.Integral

    def read(self, txt):

        assert isinstance(txt, str)
        with masking_value_error(lambda: 'Not a number: %s' % txt):
            return int(txt)

    def write(self, value):

        assert isinstance(value, self.content_type)
        return str(value)


class octal(value.type):

    content_type = numbers.Integral

    def read(self, txt):

        assert isinstance(txt, str)
        with masking_value_error(lambda: 'Not an octal number: %s' % txt):
            return int(txt, 8)

    def write(self, value):

        assert isinstance(value, self.content_type)
        return str(oct(value))


class floating_point(value.type):

    content_type = float

    def read(self, txt):

        assert isinstance(txt, str)
        with masking_value_error(lambda: 'Not a number: %s' % txt):
            return float(txt)

    def write(self, value):

        assert isinstance(value, self.content_type)
        return str(value)


class natural(subset_value.type):

    super_value = integer

    def check(self, value):

        if int(value) != value or value < 0:
            raise ValueError('Not a natural number: {0}'.format(value))
        return value
