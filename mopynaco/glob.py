
'''
    Universal file name globbing, with implicit optimization for name literals.
'''

# Copyright 2017 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['literal', 'fnmatch', 'regexp', 'predicate',
  'anydirs', 'matchings']


from os import path
import os
from fnmatch import translate
import re

from . import seq
from .Sum import Sum


class Matcher(Sum('predicate', 'literal', 'anydirs')): pass


def literal(txt):

    assert isinstance(txt, str)
    return Matcher(literal = txt)


def pattern(pat):

    r = re.compile(pat, re.MULTILINE | re.DOTALL)
    return lambda txt: r.match(txt) is not None


def fnmatch(pat):

    assert isinstance(pat, str)
    if all(c not in pat for c in '?*['):
        return Matcher(literal = pat)
    else:
        return Matcher(predicate = pattern(translate(pat)))


def regexp(pat):

    assert isinstance(pat, str)
    if re.escape(pat) == pat:
        return Matcher(literal = pat)
    else:
        return Matcher(predicate = pattern(pat + R'\Z'))


def predicate(f):

    assert callable(f)
    return Matcher(predicate = f)


anydirs = Matcher(anydirs = ())


def matchings(loc, matchers, follow_links = False):

    assert isinstance(loc, str)
    assert all(isinstance(matcher, Matcher) for matcher in matchers)

    def matchings_for(loc, matchers):

        if seq.null(matchers):
            yield []
            return

        if not path.isdir(loc) or (not follow_links and path.islink(loc)):
            return

        leading_matcher = matchers[0]
        more_matchers = matchers[1:]

        if leading_matcher.anydirs is not None:

            for name in os.listdir(loc):
                loc_sub = path.join(loc, name)
                for result in matchings_for(loc_sub, more_matchers):
                    yield [name] + result
                for result in matchings_for(loc_sub, [leading_matcher] + more_matchers):
                    yield [name] + result

        else:

            if leading_matcher.literal is not None:
                candidates = [leading_matcher.literal] \
                  if path.exists(path.join(loc, leading_matcher.literal)) \
                  else []
            else:
                candidates = [name for name in os.listdir(loc) if leading_matcher.predicate(name)]

            for name in candidates:
                for result in matchings_for(path.join(loc, name), more_matchers):
                    yield [name] + result

    if not path.exists(loc):
        return

    for result in matchings_for(loc, matchers):
        yield result
