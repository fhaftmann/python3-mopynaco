
'''
    Convenience operations on ZIP files.
'''

# Copyright 2010 - 2016 Technische Universität München

# This software may be used and distributed according to the terms of the
# GNU General Public License version 2 or any later version.
# You may obtain a copy of the GNU General Public License at:

#   http://www.gnu.org/licenses/gpl-2.0.html


__all__ = ['create', 'read', 'get_file', 'map_file',
  'map_file_in_place', 'extract']


from contextlib import contextmanager
from os import path
import os
import io
from zipfile import ZipFile, ZIP_DEFLATED, ZIP_STORED, BadZipfile

from . import exclusive


def check_path(loc):

    if path.isabs(loc):
        raise BadZipfile('Absolute path in zip archive: {}'.format(loc))
    if path.normpath(loc).startswith(os.pardir):
        raise BadZipfile('Parent path in zip archive: {}'.format(loc))


def create(dst, srcs):

    with ZipFile(dst, 'w', compression = ZIP_DEFLATED) as zip_file:
        for loc_rel, content, compressed in srcs:
            zip_file.writestr(loc_rel, content,
              ZIP_DEFLATED if compressed else ZIP_STORED)


@contextmanager
def read(loc):

    with ZipFile(loc) as zip_file:
        for entry in zip_file.infolist():
            check_path(entry.filename)
        yield zip_file


def get_file(loc, loc_rel):

    with read(loc) as zip_file:
        content = zip_file.read(loc_rel)
    return content


@contextmanager
def map_file(loc_zip_src, loc_zip_dst, loc_file):

    writer = io.BytesIO()
    with ZipFile(loc_zip_src, 'r') as zip_src:
        with zip_src.open(loc_file, 'r') as reader:
            yield reader, writer
    content = writer.getvalue()
    writer.close()

    try:
        with ZipFile(loc_zip_src, 'r') as zip_src:
            with ZipFile(loc_zip_dst, 'w') as zip_dst:
                for zip_info in zip_src.infolist():
                    if zip_info.filename == loc_file:
                        zip_dst.writestr(zip_info, content)
                    else:
                        zip_dst.writestr(zip_info, zip_src.read(zip_info.filename))
    except Exception:
        exclusive.check_and_delete(loc_zip_dst)
        raise


@contextmanager
def map_file_in_place(loc_zip, name_tmp_zip, loc_file):

    loc_tmp_zip = exclusive.mk_tmp_loc(loc_zip, name_tmp_zip)

    with map_file(loc_zip, loc_tmp_zip, loc_file) as streams:
        yield streams

    os.rename(loc_tmp_zip, loc_zip) ## atomicity asserted by POSIX


def extract(src, dst):

    with read(src) as zip_file:
        zip_file.extractall(dst)
