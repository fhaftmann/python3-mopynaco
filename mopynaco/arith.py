
'''
    Misc arithmetic.
'''

# Copyright 2013 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['assert_nat', 'assert_pos_nat', 'round_upwards', 'divide_upwards']


def assert_nat(n):

    assert isinstance(n, int)
    assert n >= 0


def assert_pos_nat(n):

    assert isinstance(n, int)
    assert n > 0


def round_upwards(m, n):

    assert_nat(m)
    assert_pos_nat(n)

    r = m % n
    return m if r == 0 else m + n - r


def divide_upwards(m, n):

    assert_nat(m)
    assert_pos_nat(n)

    q, r = divmod(m, n)
    return q if r == 0 else q + 1
