
'''
    Hierarchical properties -- data structures.
'''

# Copyright 2014 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['Props']


import functools
from collections import OrderedDict

from .. import seq
from ..Record import Record
from .. import text


sep = '.'


class Entry(Record('value', 'children')): pass


def assert_key(key):

    assert isinstance(key, str), \
      'Bad type for property key: {0}'.format(type(key))
    assert key != '' and all(c not in key for c in ' \n='), \
      'Bad property key: {0}'.format(key.replace('\n', R'\n'))


def assert_proper_value(value):

    assert isinstance(value, str), \
      'Bad type for property value: {0}'.format(type(value))
    assert '\n' not in value, \
      'Bad property value: {0}'.format(value.replace('\n', R'\n'))


def explode_key(key):

    assert_key(key)
    return key.split(sep)


def implode_key(fragments):

    (assert_key(fragment) for fragment in fragments)
    return sep.join(fragments)


class Props(object):

    def __init__(self):

        self.props = OrderedDict()

    def copy_shallow(self):

        result = type(self)()
        result.props = OrderedDict(self.props)
        return result

    def lookup_entry(self, key):

        props = self.props
        for fragment in explode_key(key):
            if fragment not in props:
                return None
            entry = props[fragment]
            props = entry.children.props
        return entry

    def __contains__(self, key):

        some_entry = self.lookup_entry(key)
        return some_entry is not None and some_entry.value is not None

    def lookup(self, key):

        some_entry = self.lookup_entry(key)
        return some_entry.value if some_entry is not None else None

    def lookup_props(self, key):

        some_entry = self.lookup_entry(key)
        return some_entry.children if some_entry is not None else type(self)()

    def map_entry_fragments(self, fragments, f):

        fragment, fragments = fragments[0], fragments[1:]
        result = self.copy_shallow()

        focus = result.props[fragment] if fragment in result.props \
          else Entry(value = None, children = Props())
        if seq.null(fragments):
            focus_result = f(focus)
        else:
            focus_result = Entry(value = focus.value,
              children = focus.children.map_entry_fragments(fragments, f))
        result.props[fragment] = focus_result

        return result

    def map_entry(self, key, f):

        assert_key(key)

        return self.map_entry_fragments(explode_key(key), f)

    def map_value(self, key, f):

        @functools.wraps(f)
        def f_(value):
            result_value = f(value)
            assert_proper_value(result_value)
            return result_value

        return self.map_entry(key, lambda entry:
          Entry(value = f_(entry.value), children = entry.children))

    def map_props(self, key, f):

        @functools.wraps(f)
        def f_(props):
            result_props = f(props)
            assert isinstance(result_props, type(self)), \
              'No properties: {0}'.format(result_props)
            return result_props

        return self.map_entry(key, lambda entry:
          Entry(value = entry.value, children = f_(entry.children)))

    def update(self, key, value):

        return self.map_value(key, lambda _: value)

    def update_props(self, key, props):

        return self.map_props(key, lambda _: props.copy_shallow())

    def remove(self, key):

        return self.map_entry(key, lambda entry:
          Entry(value = None, children = entry.children))

    def remove_props(self, key):

        return self.map_entry(key, lambda entry:
          Entry(value = entry.value, children = Props()))

    @classmethod
    def of_items(Class, items):

        def get_children(parent, fragment):
            some_entry = parent.get(fragment)
            return some_entry.children if some_entry is not None else Class()

        self = Class()

        for key, value in items:
            assert_proper_value(value)
            fragments = explode_key(key)
            last_fragment = fragments[0]
            parent = self.props
            for fragment in fragments[1:]:
                parent = parent.setdefault(last_fragment,
                  Entry(value = None, children = Class())).children.props
                last_fragment = fragment
            parent[last_fragment] = Entry(value = value,
              children = get_children(parent, last_fragment))

        return self

    def to_items(self, prefix = ''):

        for key in self.props:
            entry = self.props[key]
            sub_prefix = implode_key((prefix, key)) if prefix != '' else key
            if entry.value is not None:
                yield sub_prefix, entry.value
            for item in entry.children.to_items(sub_prefix):
                yield item

    def __iter__(self):

        return self.to_items()

    def __str__(self):

        return 'Props.of_items(' + str(list(self)) + ')'
