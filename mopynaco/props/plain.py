
'''
    Hierarchical properties -- plain (un)parsing
    routines.
'''

# Copyright 2014 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['parse', 'unparse', 'of_text', 'read', 'write']


import re

from .. import text
from .. import seq

from .Props import explode_key
from .Props import Props


re_ws = re.compile(R'^\s*(?:#.*)?$')
re_prop = re.compile(R'^([^=]+)=(.*)$')


def parse(txt, source = None, re_prop = re_prop, re_ws = re_ws, strict = True):

    assert isinstance(txt, str)
    assert source is None or isinstance(source, str)

    suffix = ' of {}'.format(source) if source is not None else ''

    def format_position(k):

        return 'at line ' + str(k + 1) + suffix

    def parse_lines(lines):

        for k, line in enumerate(lines):
            line = line.rstrip()
            if re_ws.match(line) is not None:
                yield line, None
            else:
                match = re_prop.match(line)
                if match is not None:
                    yield line, (match.group(1), match.group(2))
                elif strict:
                    raise ValueError('Bad property ' + format_position(k))

    def accumulate_positions(positions):

        pos = {}
        for key, msg in positions:
            if strict and key in pos:
                raise ValueError('Duplicate property ' + msg + ', ' + pos[key])
            pos[key] = msg
        return pos

    raw = list(parse_lines(txt.rstrip().split('\n')))

    pos = accumulate_positions((entry[0], format_position(k))
      for k, (_, entry) in enumerate(raw) if entry is not None)

    return raw, pos


def unparse(raw, items):

    # filter out non-existing keys
    keys = set(key for key, value in items)
    result = [(raw_line, some_entry) for raw_line, some_entry
      in reversed(raw) if some_entry is None or some_entry[0] in keys]

    # update or insert values
    def find_index(key):
        candidate = 0
        candidate_deviation = None
        for k, (_, some_entry) in enumerate(result):
            if some_entry is not None:
                zs, xs, ys = seq.common_prefix_suffixes(
                  explode_key(key), explode_key(some_entry[0]))
                deviation = len(xs), len(ys)
                if deviation == (0, 0):
                    return k, True
                if len(zs) > 0 and (candidate_deviation is None
                  or deviation[0] < candidate_deviation[0]
                  or (deviation[0] == candidate_deviation[0]
                  and deviation[1] < candidate_deviation[1])):
                    candidate = k
                    candidate_deviation = deviation
        return candidate, False

    for key, value in items:
        k, exact = find_index(key)
        if exact:
            if result[k][1][1] == value:
                result[k] = result[k][0], None
            else:
                result[k] = None, (key, value)
        else:
            result.insert(k, (None, (key, value)))

    # output
    lines = (some_entry[0] + '=' + some_entry[1] if raw_line is None
      else raw_line for raw_line, some_entry in reversed(result))

    return '\n'.join(lines) + '\n'


def of_text(txt, source = None, strict = True):

    raw, pos = parse(txt, source, strict = strict)
    props = Props.of_items(entry for _, entry in raw if entry is not None)
    return raw, pos, props


def read(loc, permissive = True, strict = True):

    content = text.content_utf8_of(loc, permissive)
    return ([], {}, Props()) if content is None else \
      of_text(content, source = loc, strict = strict)


def write(loc, raw, props):

    with text.writing_utf8(loc) as writer:
        writer.write(unparse(raw, props))
