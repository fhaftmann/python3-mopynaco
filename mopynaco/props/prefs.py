
'''
    Hierarchical properties -- mozilla-style prefs (un)parsing
    routines.
'''

# Copyright 2018 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['parse', 'unparse', 'read', 'write']


import re
from curses import ascii

from .. import text
from .. import seq
from ..Enum import Enum

from .Props import explode_key
from .Props import Props


re_ws = re.compile(R'^\s*(?:#.*)?$')
re_pref = re.compile(R'^user_pref\("([^"]*)",\s*(.+)\);$')


class Kind(Enum('string', 'number', 'boolean')): pass


escaping = {'\\': '\\\\', '"': '\\"', '\n': '\\n'}
escape = text.subst(escaping)
descape = text.subst({escaping[key]: key for key in escaping})
encode = text.subst({'\n': chr(ascii.US)})
decode = text.subst({chr(ascii.US): '\n'})


class Prefs:

    def __init__(self, props = None, typing = None):

        self.props = props if props is not None else Props()
        self.typing = typing if typing is not None else {}

    @staticmethod
    def parse(txt, source = None):

        assert isinstance(txt, str)
        assert source is None or isinstance(source, str)

        suffix = ' of {}'.format(source) if source is not None else ''

        def format_position(k):

            return 'at line ' + str(k + 1) + suffix

        def parse_lines(lines):

            for k, line in enumerate(lines):
                line = line.rstrip()
                matches = re_pref.match(line)
                if matches is None:
                    continue
                key = matches.group(1)
                raw_value = matches.group(2)
                if raw_value in ('false', 'true'):
                    value = 'true' if raw_value == 'true' else ''
                    kind = Kind.boolean
                elif len(raw_value) >= 2 and raw_value[0] == raw_value[-1] == '"':
                    value = encode(descape(raw_value[1:-1]))
                    kind = Kind.string
                else:
                    if str(int(raw_value)) != raw_value:
                        raise ValueError('Bad value {} {}'.format(raw_value, format_position(k)))
                    value = raw_value
                    kind = Kind.number
                yield key, (kind, value)

        raw = list(parse_lines(txt.rstrip().split('\n')))

        return Prefs(Props.of_items([(key, value) for key, (_, value) in raw]),
          {key: kind for key, (kind, _) in raw})

    @staticmethod
    def read(loc, permissive = True):

        txt = text.content_utf8_of(loc, permissive)
        return Prefs(Props(), {}) if txt is None else Prefs.parse(txt)

    def unparse(self):

        lines = []
        for key, value in self.props:
            kind = self.typing.get(key, Kind.string)
            external_value = '"' + escape(decode(value)) + '"'  if kind is Kind.string \
              else str(value) if kind is Kind.number \
              else 'true' if value == 'true' else 'false'
            lines.append('user_pref("{}", {});'.format(key, external_value))

        return '\n'.join(lines) + '\n'

    def write(self, loc):

        with text.writing_utf8(loc) as writer:
            writer.write(self.unparse())

    def update_str(self, key, value):

        return Prefs(self.props.update(key, encode(value)), self.typing)

    def update_number(self, key, value):

        return Prefs(self.props.update(key, str(value)),
          {**self.typing, **{key: Kind.number}})

    def update_boolean(self, key, value):

        return Prefs(self.props.update(key, 'true' if value else ''),
          {**self.typing, **{key: Kind.boolean}})

    def remove(self, key):

        return Prefs(self.props.remove(key),
          {k: self.typing[k] for k in self.typing if k != key})
