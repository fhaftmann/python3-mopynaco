
'''
    Dynamic handling of modules.
'''

# Copyright 2010 Technische Universität München

# This software may be used and distributed according to the terms of the
# GNU General Public License version 2 or any later version.
# You may obtain a copy of the GNU General Public License at:

#   http://www.gnu.org/licenses/gpl-2.0.html


__all__ = ['for_access', 'retrieve', 'empty', 'of_source']


import builtins
import sys
import imp

from contextlib import contextmanager


@contextmanager
def import_lock():

    imp.acquire_lock()
    try:
        yield
    finally:
        imp.release_lock()


@contextmanager
def preferring_import_path(loc = None):

    if loc is None:
        yield
    else:
        with import_lock():
            path_old = list(sys.path)
            sys.path.insert(0, loc)
            try:
                yield
            finally:
                sys.path = path_old


def name_of_access(access):

    for fragment in access:
        assert '.' not in fragment
        assert fragment != ''

    return '.'.join(access)


def for_access(access):

    if access:
        name = name_of_access(access)
        try:
            return sys.modules[name]
        except KeyError:
            raise ImportError('No module named %s.' % name)
    else:
        return builtins


def retrieve(access):

    focus = builtins
    for i in range(len(access)):
        subaccess = access[0 : i + 1]
        name = name_of_access(subaccess)
        if hasattr(focus, access[i]):
            focus = getattr(focus, access[i])
        elif name in sys.modules:
            focus = sys.modules[name]
        else:
            focus = __import__(name, fromlist = [''])
              # explicit formal fromlist ensures that __import__ returns module, not package

    return focus


def register(access, module):

    with import_lock():
        name = name_of_access(access)
        setattr(for_access(access[:-1]), access[-1], module)
        sys.modules[name] = module


def empty(access):

    with import_lock():
        name = name_of_access(access)
        module = imp.new_module(name)
        register(access, module)
        return module


def of_source(access, s, filename = '<string>', import_path = None, initial = None):

    assert access is None or len(access) >= 1

    with import_lock():
        if access is not None:
            name = name_of_access(access)
            module = imp.new_module(name)
        else:
            module = imp.new_module('anonymous')
        if initial is not None:
            module.__dict__.update(initial)
        try:
            code = compile(s, filename, 'exec')
        except TypeError as e:
            raise ImportError(str(e))
        with preferring_import_path(import_path):
            exec(code, module.__dict__)
        if access is not None:
            register(access, module)
        return module
